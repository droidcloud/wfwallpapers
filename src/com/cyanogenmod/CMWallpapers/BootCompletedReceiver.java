
package com.cyanogenmod.CMWallpapers;

import android.app.WallpaperManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class BootCompletedReceiver extends BroadcastReceiver {
    private boolean isSet = false;

    @Override
    public void onReceive(final Context context, Intent intent) {
        AsyncHandler.post(new Runnable() {

            @Override
            public void run() {
                isSet = readSeting(context);
                if (!isSet) {
                    try {
                        WallpaperManager wm = WallpaperManager.getInstance(context);
                        wm.setResource(R.drawable.wallpaper_default);
                        saveSeting(context, true);
                    } catch (java.io.IOException e) {
                    }
                }
            }
        });
    }

    private boolean readSeting(Context context) {
        SharedPreferences sharedata = context.getSharedPreferences("data", Context.MODE_PRIVATE);
        return sharedata.getBoolean("isSet", false);
    }

    private void saveSeting(Context context, boolean set) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("data",
                Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putBoolean("isSet", set);
        editor.commit();
    }
}
